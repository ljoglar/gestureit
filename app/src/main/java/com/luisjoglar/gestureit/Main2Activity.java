package com.luisjoglar.gestureit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

public class Main2Activity extends AppCompatActivity {

    // Load shared libraries on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private long timeoutValue = getTimeout();

    private float nullRejectionCoeffValue = getNullRejectionCoeff();
    private float d = 100;

    private float trimThresValue = getTrimThreshold();
    private float trimPercentValue = getTrimPercent();
    private float likelihoodValue = getLikelihoodThres();

    public static final String MyPREFERENCES = "MyPrefs" ;
    private SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private EditText editIP;
    private EditText editPort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // Preferences
        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        // Timeout
        final SeekBar timeoutSeekBar = (SeekBar) findViewById(R.id.timeoutSeekBar);
        timeoutSeekBar.setMax(3000);
        timeoutSeekBar.setProgress((int)timeoutValue);
        final TextView timeoutValueText = (TextView) findViewById(R.id.timeoutValue);
        timeoutValueText.setText(String.format(Locale.getDefault(),"%d", (int)timeoutValue));

        // Null Rejection Coefficient
        final SeekBar nullRejectionCoeffSeekBar = (SeekBar) findViewById(R.id.nullRejectionCoeffSeekBar);
        nullRejectionCoeffSeekBar.setMax(300);
        nullRejectionCoeffSeekBar.setProgress((int)nullRejectionCoeffValue * 100);
        final TextView nullRejectionCoeffValueText = (TextView) findViewById(R.id.nullRejectionCoeffValue);
        nullRejectionCoeffValueText.setText(String.format(Locale.getDefault(),"%f", nullRejectionCoeffValue));

        // Likelihood
        final SeekBar likelihoodSeekBar = (SeekBar) findViewById(R.id.likelihoodSeekBar);
        likelihoodSeekBar.setMax(100);
        likelihoodSeekBar.setProgress((int)likelihoodValue);
        final TextView likelihoodValueText = (TextView) findViewById(R.id.likelihoodValue);
        likelihoodValueText.setText(String.format(Locale.getDefault(),"%f", likelihoodValue));

        // TrimThreshold
        final SeekBar trimThresholdSeekBar = (SeekBar) findViewById(R.id.trimThresSeekBar);
        trimThresholdSeekBar.setMax(100);
        trimThresholdSeekBar.setProgress((int)trimThresValue * 100);
        final TextView trimThresValueText = (TextView) findViewById(R.id.trimThresValue);
        trimThresValueText.setText(String.format(Locale.getDefault(),"%f", trimThresValue));

        // TrimPercent
        final SeekBar trimPercentSeekBar = (SeekBar) findViewById(R.id.trimPercentSeekBar);
        trimPercentSeekBar.setMax(100);
        trimPercentSeekBar.setProgress((int)trimPercentValue);
        final TextView trimPercentValueText = (TextView) findViewById(R.id.trimPercentValue);
        trimPercentValueText.setText(String.format(Locale.getDefault(),"%d", (int)trimPercentValue));

        // OSC Config
        editIP = (EditText) findViewById(R.id.editIP);
        editPort = (EditText) findViewById(R.id.editPort);
        editIP.setText(getPreferenceS("IP"));
        editPort.setText(String.format(Locale.getDefault(),"%d", getPreferenceI("Port")));

        //Done Button
        Button buttonDone = (Button) findViewById(R.id.button);


        timeoutSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                Log.d("ljoglar2", String.format(Locale.getDefault(),"%f", progress/d));
                timeoutValueText.setText(String.format(Locale.getDefault(),"%d", progress));
                setTimeout((long)progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        nullRejectionCoeffSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                Log.d("ljoglar2", String.format(Locale.getDefault(),"%f", progress/d));
                nullRejectionCoeffValueText.setText(String.format(Locale.getDefault(),"%f", progress/d));
                setNullRejectionCoeff(progress/d);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        likelihoodSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                Log.d("ljoglar2", String.format(Locale.getDefault(),"%d", progress));
                likelihoodValueText.setText(String.format(Locale.getDefault(),"%d", progress));
                setLikelihoodThres(progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        trimThresholdSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                Log.d("ljoglar2 trimThres", String.format(Locale.getDefault(),"%f", progress/d));
                trimThresValueText.setText(String.format(Locale.getDefault(),"%f", progress/d));
                setTrimThreshold(progress/d);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        trimPercentSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                Log.d("ljoglar2", String.format(Locale.getDefault(),"%d", progress));
                trimPercentValueText.setText(String.format(Locale.getDefault(),"%d", progress));
                setTrimPercent(progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPreferenceS("IP", editIP.getText().toString());
                setPreferenceI("Port", Integer.parseInt(editPort.getText().toString()));
//                Log.d("ljoglar", "IP: " + getPreferenceS("IP"));
//                Log.d("ljoglar", "port: " + getPreferenceI("Port"));
                finish();
            }
        });
    }

    private String getPreferenceS(String name){
        return preferences.getString(name, "");
    }

    private int getPreferenceI(String name){
        return preferences.getInt(name, 0);
    }

    private void setPreferenceS (String name, String value){
        editor = preferences.edit();
        editor.putString(name, value);
        editor.apply();
    }

    private void setPreferenceI (String name, int value) {
        editor = preferences.edit();
        editor.putInt(name, value);
        editor.apply();
    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native void setTimeout(long newCoeff);
    public native long  getTimeout();
    public native void setNullRejectionCoeff(float newCoeff);
    public native float getNullRejectionCoeff();
    public native void setTrimThreshold(float newTrimThres);
    public native float getTrimThreshold();
    public native void setTrimPercent(float newTrimPercent);
    public native float getTrimPercent();
    public native void setLikelihoodThres(float newThres);
    public native float getLikelihoodThres();

}




















