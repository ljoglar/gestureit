package com.luisjoglar.gestureit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.luisjoglar.swig.DTW;
import com.luisjoglar.swig.GestureRecognitionPipeline;

import java.util.ArrayList;
import java.util.List;

import java.net.*;
import java.util.*;
import com.illposed.osc.*;



public class MainActivity extends AppCompatActivity implements SensorEventListener{

    // Load shared libraries on application startup.
    static {
        System.loadLibrary("native-lib");
    }
    static {
        System.loadLibrary("grt-lib");
    }

    // Declare global functions
    private SensorManager sensorManager;
    private Sensor accelerometer;

    private TextView textX;
    private TextView textTrain;
    private TextView textGesture;
    private TextView textTitle;
    private TextView textSubTitle;

    private boolean recording;
    private boolean gestureIt;
    private boolean isTrained;

    private GraphView graph;
    private List<Float> valuesX;
    private List<Float> valuesY;
    private List<Float> valuesZ;

    private LineGraphSeries<DataPoint> seriesX;
    private LineGraphSeries<DataPoint> seriesY;
    private LineGraphSeries<DataPoint> seriesZ;

    private final GestureRecognitionPipeline pipeline = new GestureRecognitionPipeline();

    public final String MyPREFERENCES = "MyPrefs" ;

    private SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private OSCPortOut oscPortOut;
    private String myIP;
    private int myPort;

    private boolean sendOSCMessage = false;
    private String addressOSC;
    private  ArrayList<Object> messageOSCArray = new ArrayList<Object>();

    private float likelihoodThres;

    @Override
    protected void onResume() {
        super.onResume();
        likelihoodThres = getLikelihoodThres();
        myIP = getPreferenceS("IP");
        myPort = getPreferenceI("Port");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Preferences
        preferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

         //Set the classifier at the core of the pipeline, in this case we are using a Dynamic Time Warping (DTW)
        pipeline.setClassifier(new DTW());

        // Initialise Sensors
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        int SENSOR_DELAY_CUSTOM = 30000;                                                  //delay in microseconds
        //sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, accelerometer, SENSOR_DELAY_CUSTOM);


        // Initialise Views Elements
        textTitle = (TextView) findViewById(R.id.title_text);
        textSubTitle = (TextView) findViewById(R.id.textSubTitle);
        textX = (TextView) findViewById(R.id.textX);
        textTrain = (TextView) findViewById(R.id.textTrain);
        textGesture = (TextView) findViewById(R.id.textGesture);

        Button buttonNewGesture = (Button) findViewById(R.id.button);
        Button buttonTrainGesture = (Button) findViewById(R.id.button2);
        Button buttonRecordData = (Button) findViewById(R.id.button3);
        final Button buttonGestureIt = (Button) findViewById(R.id.button4);
        Button buttonSettings = (Button) findViewById(R.id.button5);
        //Button buttonXXX = (Button) findViewById(R.id.button);

        graph = (GraphView) findViewById(R.id.graph);
        valuesX = new ArrayList<Float>();
        valuesY = new ArrayList<Float>();
        valuesZ = new ArrayList<Float>();

        drawPlot();

        // Initialise Other Variables
        recording = false;
        gestureIt = false;
        isTrained = false;

        buttonNewGesture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("ljoglar", "onClick NewGesture");
                newGestureJ();
                int maxClassLabel = getMaxClassLabel();
                textGesture.setText("Gesture" + maxClassLabel );
            }
        });

        buttonTrainGesture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("ljoglar", "onClick TrainGesture");
                  int train =  trainGestureJ();
                if(train == 1){
                    textTrain.setText("Trained");
                    isTrained = true;
                    buttonGestureIt.setText("Gesture It!");
                }
                else{
                    textTrain.setText("Error");
                }
            }
        });

        buttonRecordData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("ljoglar", "onClick RecordData");
                if(!recording){
                    startRecordingData();
                }
                else{
                    stopRecordingData();
                }
                recording = !recording;
            }
        });

        buttonGestureIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("ljoglar", "onClick TestGestures");

                // if the system is trained (isTrained == true)
                // this button starts and stop the recognition system
                if(isTrained){
                    if(!gestureIt){
                        gestureItJ();
                    }
                    else{
                        stopGestureItJ();
                    }
                    gestureIt = !gestureIt;
                }
                // else this button deletes the last recorded data
                // so it is not used to train the system
                else{
                    skipLastRecordedGesture();
                    clearPlot();
                }
            }
        });

        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("ljoglar", "onClick Settings");
                startActivity(new Intent(getApplicationContext(), Main2Activity.class));
            }
        });

        initPreferences();
    }

    //creates a new Thread to communicate with the computer and send the OSCMessages
    private Thread oscThread = new Thread() {
        @Override
        public void run() {
//            Log.d("ljoglar", "OSC run");
            /* The first part of the run() method initializes the OSCPortOut for sending messages */
            myIP = getPreferenceS("IP");
            myPort = getPreferenceI("Port");
            try {
                // Connect to some IP address and port
                oscPortOut = new OSCPortOut(InetAddress.getByName(myIP), myPort);
//                Log.d("ljoglar", "OSC connected");
            } catch (UnknownHostException e) {
                // Error handling when your IP isn't found
//                Log.d("ljoglar", "Error handling when your IP isn't found" + e);
                return;
            } catch (Exception e) {
                // Error handling for any other errors
//                Log.d("ljoglar", "Error handling for any other errors" + e);
                return;
            }

            while (true) {
                if(sendOSCMessage){
                    if (oscPortOut != null) {
                        // Creating the message
                        OSCMessage message = writeOSCMessage();
                        addressOSC = "";
                        messageOSCArray.clear();

                        try {
                            // Send the messages
                            oscPortOut.send(message);
                            sendOSCMessage = false;
                            Log.d("ljoglar", "OSC Message");
                        } catch (Exception e) {
                            // Error handling for some error
                        }
                    }
                }
            }
        }
    };

    private OSCMessage writeOSCMessage(){
        return new OSCMessage(addressOSC, messageOSCArray);
    }

    private void writeContentOSCMessage(int gestureLabel){
        addressOSC = "/gesture" + String.format(Locale.getDefault(),"%d", gestureLabel);
    }

    private void initPreferences(){
        editor = preferences.edit();
        editor.putString("Ip","192.168.0.8");
        editor.putInt("Port", 8000);
        editor.apply();
    }

    private String getPreferenceS(String name){
        return preferences.getString(name, "");
    }

    private int getPreferenceI(String name){
        return preferences.getInt(name, 0);
    }

    private void newGestureJ(){  newGesture();  }

    private int trainGestureJ(){
        return trainGesture();
    }

    private void startRecordingData() {
        textX.setText("Recording Data");
        valuesX = new ArrayList<>();
        valuesY = new ArrayList<>();
        valuesZ = new ArrayList<>();
    }

    private void stopRecordingData(){
        textX.setText("Stop");
        stopRecording();
        redrawPlot(valuesX, valuesY, valuesZ);
    }

    private void gestureItJ(){
        textTitle.setText("Gesture It! Running");
        oscThread.start();
        likelihoodThres = getLikelihoodThres();
//        testRealTime();
    }

    // not used right now.
    //TODO: create system to be able to stop and resume the recognition
    private void stopGestureItJ(){
        textTitle.setText("Gesture It! Stopped");
    }


    // creates a dummy plot to initialise the graphView in onCreate()
    private void drawPlot(){
        seriesX = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 1),
                new DataPoint(1, 5),
                new DataPoint(2, 3),
                new DataPoint(3, 2),
                new DataPoint(4, 6)
        });
        seriesY = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 7),
                new DataPoint(1, 4),
                new DataPoint(2, 6),
                new DataPoint(3, 2),
                new DataPoint(4, 4)
        });
        seriesZ = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 7),
                new DataPoint(1, 2),
                new DataPoint(2, 5),
                new DataPoint(3, 3),
                new DataPoint(4, 8)
        });
        seriesX.setColor(Color.BLUE);
        seriesY.setColor(Color.GREEN);
        seriesZ.setColor(Color.RED);

        graph.addSeries(seriesX);
        graph.addSeries(seriesY);
        graph.addSeries(seriesZ);
    }

    private void redrawPlot(List<Float> valuesX, List<Float> valuesY, List<Float> valuesZ){
        DataPoint[] arrayX = new DataPoint[valuesX.size()];
        DataPoint[] arrayY = new DataPoint[valuesY.size()];
        DataPoint[] arrayZ = new DataPoint[valuesZ.size()];

        for (int i = 0; i < valuesX.size(); i++){
            DataPoint auxX = new DataPoint(i, valuesX.get(i));
            DataPoint auxY = new DataPoint(i, valuesY.get(i));
            DataPoint auxZ = new DataPoint(i, valuesZ.get(i));
            arrayX[i] = auxX;
            arrayY[i] = auxY;
            arrayZ[i] = auxZ;
        }
        seriesX.resetData(arrayX);
        seriesY.resetData(arrayY);
        seriesZ.resetData(arrayZ);
    }

    // creates a plot where all the dimensions have only a point (0,0)
    private void clearPlot(){
        DataPoint[] array = new DataPoint[1];
        DataPoint aux = new DataPoint(0, 0);
        array[0] = aux;
        seriesX.resetData(array);
        seriesY.resetData(array);
        seriesZ.resetData(array);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
       if(recording){
           readAccelerometerData(event.values[0], event.values[1], event.values[2]);
           valuesX.add(event.values[0]);
           valuesY.add(event.values[1]);
           valuesZ.add(event.values[2]);
       }
       else if(gestureIt){
           int gestureLabel = gestureIt(event.values[0], event.values[1], event.values[2]);
           if(gestureLabel != 0){
               float likelihood = getLikelihoodLastGesture();
//               Log.d("ljoglar2", "likelihood:" + String.format(Locale.getDefault(),"%f", likelihood));
//               Log.d("ljoglar2", "likelihoodThres:" + String.format(Locale.getDefault(),"%f", likelihoodThres));
               if(likelihood > likelihoodThres/100){
                   textSubTitle.setText("Gesture: " + gestureLabel + "  - Likelihood: " + likelihood );
                   writeContentOSCMessage(gestureLabel);
                   sendOSCMessage = true;
               }
           }
       }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native void readAccelerometerData(float accX, float accY, float accZ);
    public native void stopRecording();
    public native void skipLastRecordedGesture();
    public native int trainGesture();
    public native void newGesture();
    public native int gestureIt(float accX, float accY, float accZ);
    public native int getMaxClassLabel();
    public native float getLikelihoodLastGesture();
    public native float getLikelihoodThres();
//    public native void testRealTime();

}

