/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.8
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.luisjoglar.swig;

public interface grtConstants {
  public final static double PI = grtJNI.PI_get();
  public final static double TWO_PI = grtJNI.TWO_PI_get();
  public final static double ONE_OVER_TWO_PI = grtJNI.ONE_OVER_TWO_PI_get();
  public final static double SQRT_TWO_PI = grtJNI.SQRT_TWO_PI_get();
  public final static int GRT_DEFAULT_NULL_CLASS_LABEL = grtJNI.GRT_DEFAULT_NULL_CLASS_LABEL_get();
  public final static boolean GRT_SAFE_CHECKING = grtJNI.GRT_SAFE_CHECKING_get();
  public final static int DEFAULT_NULL_LIKELIHOOD_VALUE = grtJNI.DEFAULT_NULL_LIKELIHOOD_VALUE_get();
  public final static int DEFAULT_NULL_DISTANCE_VALUE = grtJNI.DEFAULT_NULL_DISTANCE_VALUE_get();
  public final static int INSERT_AT_END_INDEX = grtJNI.INSERT_AT_END_INDEX_get();
}
