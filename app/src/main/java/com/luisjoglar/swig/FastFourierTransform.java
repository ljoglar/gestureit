/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.8
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.luisjoglar.swig;

public class FastFourierTransform extends GRTBase {
  private transient long swigCPtr;

  protected FastFourierTransform(long cPtr, boolean cMemoryOwn) {
    super(grtJNI.FastFourierTransform_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FastFourierTransform obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        grtJNI.delete_FastFourierTransform(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public FastFourierTransform() {
    this(grtJNI.new_FastFourierTransform__SWIG_0(), true);
  }

  public FastFourierTransform(FastFourierTransform rhs) {
    this(grtJNI.new_FastFourierTransform__SWIG_1(FastFourierTransform.getCPtr(rhs), rhs), true);
  }

  public boolean init(long windowSize, long windowFunction, boolean computeMagnitude, boolean computePhase, boolean enableZeroPadding) {
    return grtJNI.FastFourierTransform_init__SWIG_0(swigCPtr, this, windowSize, windowFunction, computeMagnitude, computePhase, enableZeroPadding);
  }

  public boolean init(long windowSize, long windowFunction, boolean computeMagnitude, boolean computePhase) {
    return grtJNI.FastFourierTransform_init__SWIG_1(swigCPtr, this, windowSize, windowFunction, computeMagnitude, computePhase);
  }

  public boolean init(long windowSize, long windowFunction, boolean computeMagnitude) {
    return grtJNI.FastFourierTransform_init__SWIG_2(swigCPtr, this, windowSize, windowFunction, computeMagnitude);
  }

  public boolean init(long windowSize, long windowFunction) {
    return grtJNI.FastFourierTransform_init__SWIG_3(swigCPtr, this, windowSize, windowFunction);
  }

  public boolean init(long windowSize) {
    return grtJNI.FastFourierTransform_init__SWIG_4(swigCPtr, this, windowSize);
  }

  public boolean computeFFT(VectorFloat data) {
    return grtJNI.FastFourierTransform_computeFFT(swigCPtr, this, VectorFloat.getCPtr(data), data);
  }

  public VectorFloat getMagnitudeData() {
    return new VectorFloat(grtJNI.FastFourierTransform_getMagnitudeData(swigCPtr, this), true);
  }

  public VectorFloat getPhaseData() {
    return new VectorFloat(grtJNI.FastFourierTransform_getPhaseData(swigCPtr, this), true);
  }

  public VectorFloat getPowerData() {
    return new VectorFloat(grtJNI.FastFourierTransform_getPowerData(swigCPtr, this), true);
  }

  public double getAveragePower() {
    return grtJNI.FastFourierTransform_getAveragePower(swigCPtr, this);
  }

  public SWIGTYPE_p_double getMagnitudeDataPtr() {
    long cPtr = grtJNI.FastFourierTransform_getMagnitudeDataPtr(swigCPtr, this);
    return (cPtr == 0) ? null : new SWIGTYPE_p_double(cPtr, false);
  }

  public SWIGTYPE_p_double getPhaseDataPtr() {
    long cPtr = grtJNI.FastFourierTransform_getPhaseDataPtr(swigCPtr, this);
    return (cPtr == 0) ? null : new SWIGTYPE_p_double(cPtr, false);
  }

  public SWIGTYPE_p_double getPowerDataPtr() {
    long cPtr = grtJNI.FastFourierTransform_getPowerDataPtr(swigCPtr, this);
    return (cPtr == 0) ? null : new SWIGTYPE_p_double(cPtr, false);
  }

  public long getFFTSize() {
    return grtJNI.FastFourierTransform_getFFTSize(swigCPtr, this);
  }

  public final static class WindowFunctionOptions {
    public final static FastFourierTransform.WindowFunctionOptions RECTANGULAR_WINDOW = new FastFourierTransform.WindowFunctionOptions("RECTANGULAR_WINDOW", grtJNI.FastFourierTransform_RECTANGULAR_WINDOW_get());
    public final static FastFourierTransform.WindowFunctionOptions BARTLETT_WINDOW = new FastFourierTransform.WindowFunctionOptions("BARTLETT_WINDOW");
    public final static FastFourierTransform.WindowFunctionOptions HAMMING_WINDOW = new FastFourierTransform.WindowFunctionOptions("HAMMING_WINDOW");
    public final static FastFourierTransform.WindowFunctionOptions HANNING_WINDOW = new FastFourierTransform.WindowFunctionOptions("HANNING_WINDOW");

    public final int swigValue() {
      return swigValue;
    }

    public String toString() {
      return swigName;
    }

    public static WindowFunctionOptions swigToEnum(int swigValue) {
      if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
        return swigValues[swigValue];
      for (int i = 0; i < swigValues.length; i++)
        if (swigValues[i].swigValue == swigValue)
          return swigValues[i];
      throw new IllegalArgumentException("No enum " + WindowFunctionOptions.class + " with value " + swigValue);
    }

    private WindowFunctionOptions(String swigName) {
      this.swigName = swigName;
      this.swigValue = swigNext++;
    }

    private WindowFunctionOptions(String swigName, int swigValue) {
      this.swigName = swigName;
      this.swigValue = swigValue;
      swigNext = swigValue+1;
    }

    private WindowFunctionOptions(String swigName, WindowFunctionOptions swigEnum) {
      this.swigName = swigName;
      this.swigValue = swigEnum.swigValue;
      swigNext = this.swigValue+1;
    }

    private static WindowFunctionOptions[] swigValues = { RECTANGULAR_WINDOW, BARTLETT_WINDOW, HAMMING_WINDOW, HANNING_WINDOW };
    private static int swigNext = 0;
    private final int swigValue;
    private final String swigName;
  }

}
