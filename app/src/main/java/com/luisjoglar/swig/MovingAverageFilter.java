/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.8
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.luisjoglar.swig;

public class MovingAverageFilter extends PreProcessing {
  private transient long swigCPtr;

  protected MovingAverageFilter(long cPtr, boolean cMemoryOwn) {
    super(grtJNI.MovingAverageFilter_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(MovingAverageFilter obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        grtJNI.delete_MovingAverageFilter(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public MovingAverageFilter(long filterSize, long numDimensions) {
    this(grtJNI.new_MovingAverageFilter__SWIG_0(filterSize, numDimensions), true);
  }

  public MovingAverageFilter(long filterSize) {
    this(grtJNI.new_MovingAverageFilter__SWIG_1(filterSize), true);
  }

  public MovingAverageFilter() {
    this(grtJNI.new_MovingAverageFilter__SWIG_2(), true);
  }

  public MovingAverageFilter(MovingAverageFilter rhs) {
    this(grtJNI.new_MovingAverageFilter__SWIG_3(MovingAverageFilter.getCPtr(rhs), rhs), true);
  }

  public boolean deepCopyFrom(PreProcessing preProcessing) {
    return grtJNI.MovingAverageFilter_deepCopyFrom(swigCPtr, this, PreProcessing.getCPtr(preProcessing), preProcessing);
  }

  public boolean process(VectorFloat inputVector) {
    return grtJNI.MovingAverageFilter_process(swigCPtr, this, VectorFloat.getCPtr(inputVector), inputVector);
  }

  public boolean reset() {
    return grtJNI.MovingAverageFilter_reset(swigCPtr, this);
  }

  public boolean save(SWIGTYPE_p_std__fstream file) {
    return grtJNI.MovingAverageFilter_save__SWIG_0(swigCPtr, this, SWIGTYPE_p_std__fstream.getCPtr(file));
  }

  public boolean load(SWIGTYPE_p_std__fstream file) {
    return grtJNI.MovingAverageFilter_load__SWIG_0(swigCPtr, this, SWIGTYPE_p_std__fstream.getCPtr(file));
  }

  public boolean init(long filterSize, long numDimensions) {
    return grtJNI.MovingAverageFilter_init(swigCPtr, this, filterSize, numDimensions);
  }

  public double filter(double x) {
    return grtJNI.MovingAverageFilter_filter__SWIG_0(swigCPtr, this, x);
  }

  public VectorFloat filter(VectorFloat x) {
    return new VectorFloat(grtJNI.MovingAverageFilter_filter__SWIG_1(swigCPtr, this, VectorFloat.getCPtr(x), x), true);
  }

  public long getFilterSize() {
    return grtJNI.MovingAverageFilter_getFilterSize(swigCPtr, this);
  }

  public VectorFloat getFilteredData() {
    return new VectorFloat(grtJNI.MovingAverageFilter_getFilteredData(swigCPtr, this), true);
  }

  public static SWIGTYPE_p_std__string getId() {
    return new SWIGTYPE_p_std__string(grtJNI.MovingAverageFilter_getId(), true);
  }

}
