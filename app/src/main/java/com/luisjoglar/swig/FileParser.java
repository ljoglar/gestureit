/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.8
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.luisjoglar.swig;

public class FileParser {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected FileParser(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FileParser obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        grtJNI.delete_FileParser(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public FileParser() {
    this(grtJNI.new_FileParser(), true);
  }

  public boolean parseCSVFile(SWIGTYPE_p_std__string filename, boolean removeNewLineCharacter) {
    return grtJNI.FileParser_parseCSVFile__SWIG_0(swigCPtr, this, SWIGTYPE_p_std__string.getCPtr(filename), removeNewLineCharacter);
  }

  public boolean parseCSVFile(SWIGTYPE_p_std__string filename) {
    return grtJNI.FileParser_parseCSVFile__SWIG_1(swigCPtr, this, SWIGTYPE_p_std__string.getCPtr(filename));
  }

  public boolean parseTSVFile(SWIGTYPE_p_std__string filename, boolean removeNewLineCharacter) {
    return grtJNI.FileParser_parseTSVFile__SWIG_0(swigCPtr, this, SWIGTYPE_p_std__string.getCPtr(filename), removeNewLineCharacter);
  }

  public boolean parseTSVFile(SWIGTYPE_p_std__string filename) {
    return grtJNI.FileParser_parseTSVFile__SWIG_1(swigCPtr, this, SWIGTYPE_p_std__string.getCPtr(filename));
  }

  public boolean getFileParsed() {
    return grtJNI.FileParser_getFileParsed(swigCPtr, this);
  }

  public boolean getConsistentColumnSize() {
    return grtJNI.FileParser_getConsistentColumnSize(swigCPtr, this);
  }

  public long getRowSize() {
    return grtJNI.FileParser_getRowSize(swigCPtr, this);
  }

  public long getColumnSize() {
    return grtJNI.FileParser_getColumnSize(swigCPtr, this);
  }

  public SWIGTYPE_p_std__dequeT_VectorT_std__string_t_t getFileContents() {
    return new SWIGTYPE_p_std__dequeT_VectorT_std__string_t_t(grtJNI.FileParser_getFileContents(swigCPtr, this), false);
  }

  public boolean clear() {
    return grtJNI.FileParser_clear(swigCPtr, this);
  }

  public static boolean parseColumn(SWIGTYPE_p_std__string row, SWIGTYPE_p_VectorT_std__string_t cols, char seperator) {
    return grtJNI.FileParser_parseColumn__SWIG_0(SWIGTYPE_p_std__string.getCPtr(row), SWIGTYPE_p_VectorT_std__string_t.getCPtr(cols), seperator);
  }

  public static boolean parseColumn(SWIGTYPE_p_std__string row, SWIGTYPE_p_VectorT_std__string_t cols) {
    return grtJNI.FileParser_parseColumn__SWIG_1(SWIGTYPE_p_std__string.getCPtr(row), SWIGTYPE_p_VectorT_std__string_t.getCPtr(cols));
  }

}
