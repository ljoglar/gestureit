
#include <GRT.h>
#include <string>
using namespace GRT;
using namespace std;

#include <jni.h>
#include <errno.h>
#include <string>

#include <EGL/egl.h>
#include <GLES/gl.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>



#define  LOG_TAG    "native-activity"
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
// LOGD("Hello world") or LOGE("Number = %d", any_int) like printf in c.


////** Gesture Class **////

class Gesture{
    int dimensions = 3;
    GestureRecognitionPipeline pipeline;
    TimeSeriesClassificationData trainingData;
    DTW dtw;
    UINT maxClassLabel;
    MatrixFloat trainingSample;
    float trimThres = 0.1;
    float trimPercent = 50;
    unsigned long timeOut = 1000;
    float likelihoodThreshold = 60;


public:
    Gesture();
    int getDimensions();
    UINT getPredictedClassLabel();
    Float getMaximumLikelihood();
    void setTrainingSet(VectorFloat sample);
    void setTimeout(unsigned long newTime);
    unsigned long getTimeout();
    void setNullRejectionCoeff(float newCoeff);
    float getNullRejectionCoeff();
    void setTrimThreshold(float newThres);
    float getTrimThreshold();
    void setTrimPercent(float newPercent);
    float getTrimPercent();
    float getLikelihoodThres();
    void setLikelihoodThres(float newThres);
    UINT getMaxClassLabel();

    void enableTrimTrainingSamples();
    void saveAndResetTrainingSample();
    bool trainGesture();
    void updateMaxClassLabel();
    void removeLastSample();
    bool gotTrained();
    bool predict(VectorFloat sample);
};


Gesture::Gesture() {
//    LOGD("init Gesture");
    trainingData.setNumDimensions(dimensions);
    maxClassLabel = 1;

    //Turn on null rejection, this lets the classifier output the predicted class label of 0 when the likelihood of a gesture is low
    dtw.enableNullRejection( true );

    //Set the null rejection coefficient to 3, this controls the thresholds for the automatic null rejection
    //You can increase this value if you find that your real-time gestures are not being recognized
    //If you are getting too many false positives then you should decrease this value
    dtw.setNullRejectionCoeff(1.2);
    //dtw.setNullRejectionThreshold(1.5);

    //Turn on the automatic data trimming, this will remove any sections of none movement from the start and end of the training samples
    dtw.enableTrimTrainingData(true, trimThres, trimPercent);

    //Offset the timeseries data by the first sample, this makes your gestures (more) invariant to the location the gesture is performed
    dtw.setOffsetTimeseriesUsingFirstSample(true);


    //Set the classifier at the core of the pipeline
    pipeline.setClassifier(dtw);

    //Add a class label timeout filter to the end of the pipeline (with a timeout value of 1 second), this will filter the predicted class output from the ANBC algorithm
    pipeline.addPostProcessingModule(ClassLabelTimeoutFilter(timeOut));
}

int Gesture::getDimensions(){
    return dimensions;
}

UINT Gesture::getPredictedClassLabel(){
    return pipeline.getPredictedClassLabel();
}

Float Gesture::getMaximumLikelihood(){
    return pipeline.getMaximumLikelihood();
}

void Gesture::setTimeout(unsigned long newTime){
    timeOut = newTime;
//    LOGI("new TimeOut: %d", timeOut);
    pipeline.removeAllPostProcessingModules();
    pipeline.addPostProcessingModule(ClassLabelTimeoutFilter(newTime));
}

unsigned long Gesture::getTimeout(){
    return timeOut;
}

void Gesture::setNullRejectionCoeff(float newCoeff){
    //LOGI("NullRejectionCoeff: %f", newCoeff);
    Float coeff = newCoeff;
    dtw.setNullRejectionCoeff(coeff);
}

float Gesture::getNullRejectionCoeff(){
    return (float)dtw.getNullRejectionCoeff();
}

void Gesture::setTrimThreshold(float newThres){
    trimThres = newThres;
    enableTrimTrainingSamples();
}

float Gesture::getTrimThreshold(){
    return trimThres;
}

void Gesture::setTrimPercent(float newPercent){
    trimPercent = newPercent;
    enableTrimTrainingSamples();
}

float Gesture::getTrimPercent(){
    return trimPercent;
}

void Gesture::enableTrimTrainingSamples(){
    dtw.enableTrimTrainingData(true, trimThres, trimPercent);
}

float Gesture::getLikelihoodThres(){
    return likelihoodThreshold;
}
void Gesture::setLikelihoodThres(float newThres){
    likelihoodThreshold = newThres;
}

UINT Gesture::getMaxClassLabel(){
    return maxClassLabel;
}

void Gesture::saveAndResetTrainingSample(){
//    LOGD("saveAndResetTrainingSample");
    trainingData.addSample( maxClassLabel, trainingSample);
    trainingSample.clear();
}

void Gesture::setTrainingSet(VectorFloat sample) {
    //LOGD("setTrainingSet");
    trainingSample.push_back(sample);
}

bool Gesture::trainGesture() {
//    LOGD("trainingGesture");
    bool successTrain = pipeline.train(trainingData);
    if(successTrain) dtw.recomputeNullRejectionThresholds();
    return successTrain;
//    LOGE("trained %d", successTrain ? 1 : 0 );
}

void Gesture::updateMaxClassLabel(){
//    LOGD("updateMaxClassLabel");
    maxClassLabel++;
}

void Gesture::removeLastSample(){
    trainingData.removeLastSample();
}

bool Gesture::gotTrained(){
    return pipeline.getTrained();
}

bool Gesture::predict(VectorFloat sample){
    return pipeline.predict(sample);
}


////** End Gesture Class **////




////*** Main code ***////

Gesture gesture;

////*** End Main code ***////



////**** Utils C++ <-> Java ****////

////**** MainActivity ****////

// TODO: Try to use sensors from Native Library
extern "C"
void Java_com_luisjoglar_gestureit_MainActivity_readAccelerometerData(JNIEnv *env, float dummy, float accX, float accY, float accZ) {
    (void) env;
    //LOGD("readSensorData");
    //LOGI("accelerometer: x=%f y=%f z=%f", accX, accY, accZ);
    int dim = gesture.getDimensions();
    VectorFloat sample(dim);
    for(int i = 0; i < dim ; i++){
        switch (i){
            case 0:
                sample[i] = accX;
                break;
            case 1:
                sample[i] = accY;
                break;
            case 2:
                sample[i] = accZ;
                break;
        }
    }
    gesture.setTrainingSet(sample);
}

extern "C"
void Java_com_luisjoglar_gestureit_MainActivity_stopRecording(JNIEnv *env) {
    (void) env;
//    LOGD("stopRecording");
    gesture.saveAndResetTrainingSample();
}

extern "C"
void Java_com_luisjoglar_gestureit_MainActivity_skipLastRecordedGesture(JNIEnv *env) {
    gesture.removeLastSample();
}

extern "C"
int Java_com_luisjoglar_gestureit_MainActivity_trainGesture(JNIEnv *env) {
    (void) env;
//    LOGD("trainGesture");
    if(gesture.trainGesture()){
        return 1;
    }
    else{
        return 0;
    }
}

extern "C"
void Java_com_luisjoglar_gestureit_MainActivity_newGesture(JNIEnv *env) {
//    LOGD("newGesture");
    gesture.updateMaxClassLabel();
}

extern "C"
int Java_com_luisjoglar_gestureit_MainActivity_gestureIt(JNIEnv *env, float dummy, float accX, float accY, float accZ) {
    (void) env;

    int dim = gesture.getDimensions();
    VectorFloat sample(dim);
    for(int i = 0; i < dim ; i++){
        switch (i){
            case 0:
                sample[i] = accX;
                break;
            case 1:
                sample[i] = accY;
                break;
            case 2:
                sample[i] = accZ;
                break;
        }
    }

    UINT predictedClassLabel = 0;
//    Float maxLikelihood = 0;

    if( gesture.gotTrained() ){
        gesture.predict( sample );
        predictedClassLabel = gesture.getPredictedClassLabel();
//        maxLikelihood = gesture.getMaximumLikelihood();
//        LOGE("Likelihood: %f", maxLikelihood);
    }
    return predictedClassLabel;
}

extern "C"
int Java_com_luisjoglar_gestureit_MainActivity_getMaxClassLabel(){
    return gesture.getMaxClassLabel();
}

extern "C"
float Java_com_luisjoglar_gestureit_MainActivity_getLikelihoodLastGesture(JNIEnv *env) {
    (void) env;
    return (float)gesture.getMaximumLikelihood();
}

extern "C"
float Java_com_luisjoglar_gestureit_MainActivity_getLikelihoodThres(JNIEnv *env) {
    (void) env;
    return gesture.getLikelihoodThres();
}

//extern "C"
//void Java_com_luisjoglar_gestureit_MainActivity_testRealTime(JNIEnv *env) {
//    (void) env;
//    LOGD("testRealTime");
//    // restart count
//    gesture.setLastPredictedClass(0);
//    gesture.setCountClassConsec(1);
//}


////**** Main Activity 2 ****////

extern "C"
void Java_com_luisjoglar_gestureit_Main2Activity_setTimeout(JNIEnv *env, float dummy,  unsigned long newCoeff) {
    (void) env;
    gesture.setTimeout(newCoeff);
}

extern "C"
unsigned long Java_com_luisjoglar_gestureit_Main2Activity_getTimeout(JNIEnv *env) {
    (void) env;
    return gesture.getTimeout();
}

extern "C"
void Java_com_luisjoglar_gestureit_Main2Activity_setNullRejectionCoeff(JNIEnv *env, float dummy,  float newCoeff) {
    (void) env;
//    LOGI("NullRejectionCoeff: %f", newCoeff);
    gesture.setNullRejectionCoeff(newCoeff);
}

extern "C"
float Java_com_luisjoglar_gestureit_Main2Activity_getNullRejectionCoeff(JNIEnv *env) {
    (void) env;
    return gesture.getNullRejectionCoeff();
}

extern "C"
void Java_com_luisjoglar_gestureit_Main2Activity_setTrimThreshold(JNIEnv *env, float dummy,  float newThres) {
    (void) env;
//    LOGI("TrimThreshold: %f", newThres);
    gesture.setTrimThreshold(newThres);
}

extern "C"
float Java_com_luisjoglar_gestureit_Main2Activity_getTrimThreshold(JNIEnv *env) {
    (void) env;
    return gesture.getTrimThreshold();
}

extern "C"
void Java_com_luisjoglar_gestureit_Main2Activity_setTrimPercent(JNIEnv *env, float dummy,  float newPercent) {
    (void) env;
//    LOGI("TrimPercent: %f", newPercent);
    gesture.setTrimPercent(newPercent);
}

extern "C"
float Java_com_luisjoglar_gestureit_Main2Activity_getTrimPercent(JNIEnv *env) {
    (void) env;
    return gesture.getTrimPercent();
}

extern "C"
void Java_com_luisjoglar_gestureit_Main2Activity_setLikelihoodThres(JNIEnv *env, float dummy,  float newThres) {
    (void) env;
//    LOGI("TrimPercent: %f", newThres);
    gesture.setLikelihoodThres(newThres);
}

extern "C"
float Java_com_luisjoglar_gestureit_Main2Activity_getLikelihoodThres(JNIEnv *env) {
    (void) env;
    return gesture.getLikelihoodThres();
}


////**** End Utils Java-C++ ****////

