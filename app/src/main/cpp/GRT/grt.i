%module grt

%{
#include "GRT.h"
using namespace GRT;
%}

%inline %{
typedef unsigned int UINT;
%}

%rename(assign) GRT::IndexDist::operator=;
%rename(assign) GRT::DTW::operator=;

%include "Util/GRTTypedefs.h"
%include "Util/GRTException.h"
%include "Util/Log.h"
%include "Util/ErrorLog.h"
%include "Util/DebugLog.h"
%include "Util/WarningLog.h"
%include "Util/MinMax.h"
%include "Util/FileParser.h"

%rename(assign) GRT::TimeSeriesClassificationSample::operator=;
%rename(get) GRT::TimeSeriesClassificationSample::operator[];
%include "DataStructures/TimeSeriesClassificationSample.h"

%rename(assign) GRT::TimeSeriesClassificationData::operator=;
%rename(get) GRT::TimeSeriesClassificationData::operator[];
%include "DataStructures/TimeSeriesClassificationData.h"

%include "DataStructures/MatrixFloat.h"
%include "DataStructures/Matrix.h"
%include "DataStructures/Vector.h"
%include "DataStructures/VectorFloat.h"

%include "CoreModules/Classifier.h"
%include "ClassificationModules/DTW/DTW.h"

%include "CoreModules/GestureRecognitionPipeline.h"
%include "CoreModules/PreProcessing.h"
%include "CoreModules/PostProcessing.h"
%include "CoreModules/GRTBase.h"
%include "CoreModules/FeatureExtraction.h"

%include "PreProcessingModules/MovingAverageFilter.h"

%include "FeatureExtractionModules/FFT/FastFourierTransform.h"
%include "FeatureExtractionModules/FFT/FFT.h"

%include "PostProcessingModules/ClassLabelFilter.h"
%include "PostProcessingModules/ClassLabelChangeFilter.h"
%include "PostProcessingModules/ClassLabelTimeoutFilter.h"
